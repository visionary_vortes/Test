<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
            'name' => 'rohit',
            'email' => 'rohit@gmail.com',
            'password' => bcrypt('password')
        ];
        Admin::create($admin);

    }
}
